﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electronic_Retail_Utility___by_GNBR.CO
{
    public partial class PointofsaleForm : Form
    {
        public PointofsaleForm()
        {
            InitializeComponent();
        }

        private void PointofsaleForm_Load(object sender, EventArgs e)
        {
            MainForm mainform = new MainForm();
            int xlocation, ylocation, widthResolution, heightResolution;
            widthResolution = Screen.PrimaryScreen.WorkingArea.Width;
            heightResolution = Screen.PrimaryScreen.WorkingArea.Height;
            xlocation = (widthResolution - mainform.Size.Width)/2 + 258;
            ylocation = (heightResolution - this.Size.Height) / 2;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(xlocation, ylocation);

        }

        private void SelectItemlabel_Click(object sender, EventArgs e)
        {
            selectItemtextBox.Focus();
        }

        private void enterQuantitylabel_Click(object sender, EventArgs e)
        {
            enterQuantitytextBox.Focus();
        }

        private void selectItemtextBox_TextChanged(object sender, EventArgs e)
        {
            if (selectItemtextBox.Text != "")
            {
                SelectItemlabel.Visible = false;
            }
            else
            {
                SelectItemlabel.Visible = true;
            }
        }

        private void enterQuantitytextBox_TextChanged(object sender, EventArgs e)
        {
            if (enterQuantitytextBox.Text != "")
            {
                enterQuantitylabel.Visible = false;
            }
            else
            {
                enterQuantitylabel.Visible = true;
            }
        }
    }
}
