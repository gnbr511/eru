﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electronic_Retail_Utility___by_GNBR.CO
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Xbutton_Click(object sender, EventArgs e)
        {
            DialogResult messageResponse;
            messageResponse = MessageBox.Show("Click 'Yes' to close the system.", "Close the system?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (messageResponse == DialogResult.Yes) {                
                this.Close();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            String a;
            a = System.DateTime.Now.Date.ToString("dd-MM-yyyy");
            dateTimelabel.Text = a + "  " + String.Format("{0:hh:mm:ss tt}", System.DateTime.Now);
        }

        private void PointOfSalebutton_Click(object sender, EventArgs e)
        {
            PointofsaleForm pointofsaleform = new PointofsaleForm();
            pointofsaleform.Show();
        }
    }
}
