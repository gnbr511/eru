﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Electronic_Retail_Utility___by_GNBR.CO
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void userNametextBox_TextChanged(object sender, EventArgs e)
        {
            if (userNametextBox.Text != "")
            {
                enterUsernamelabel.Visible = false;
            }
            else {
                enterUsernamelabel.Visible = true;
            }
        }

        private void passWordtextBox_TextChanged(object sender, EventArgs e)
        {
            if (passWordtextBox.Text != "")
            {
                enterPasswordlabel.Visible = false;
            }
            else
            {
                enterPasswordlabel.Visible = true;
            }
        }

        private void loginbutton_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainform = new MainForm();
            mainform.ShowDialog();
            this.Close();                      
        }

        private void enterUsernamelabel_Click(object sender, EventArgs e)
        {
            userNametextBox.Focus();
        }

        private void enterPasswordlabel_Click(object sender, EventArgs e)
        {
            passWordtextBox.Focus();
        }
    }
}
