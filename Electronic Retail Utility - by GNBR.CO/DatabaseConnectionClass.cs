﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electronic_Retail_Utility___by_GNBR.CO
{
    class DatabaseConnectionClass
    {
        public MySqlConnection mysqlcon;
        public String query;
        public MySqlCommand command;
        public MySqlDataReader reader;

        public String host = "localhost";
        public String database = "electronic_retail_utility";
        public String user = "root";
        public String pass = "";

        public void connectToDatabase() {
            try
            {
                mysqlcon = new MySqlConnection();
                mysqlcon.ConnectionString = "server = '" + host +
                    "database = '" + database +
                    "username = '" + user +
                    "password = '" + pass + "'";
                mysqlcon.Open();
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}
